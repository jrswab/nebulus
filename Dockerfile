FROM ubuntu:18.10
LABEL maintainer @jrswab

RUN apt-get update && apt-get install -y \
	wget \
	build-essential \
	libssl-dev \
	python3-dev \
	python3-pip \
	&& pip3 install --no-cache-dir \
		--upgrade setuptools \
	&& pip3 install cryptography \
	&& pip3 install beem \
	&& rm -rf /var/lib/apt/lists/*

COPY node/ /root/
WORKDIR /root/

EXPOSE 4001
EXPOSE 5001

ENTRYPOINT ["bash", "ipfs_run.sh"]
