#!/bin/bash

# check for user defined ports
if [ $# -eq 0 ]; then
	portA=65037
	portB=39062
elif [ $# -eq 1 ]; then
	portA=$1
	portB=39062
elif [ $# -eq 2 ]; then
	portA=$1
	portB=$2
fi

# Grab latest Docker image
docker pull jrswab/nebulus:latest

# run docker
loc="$(readlink -f storage)"
docker container run -it \
	-v ${loc}:/root/.ipfs \
	-p $portA:4001 -p $portB:5001 \
	--name nebulus \
	--rm jrswab/nebulus:latest
