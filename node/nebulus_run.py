#!/usr/bin/env python3

from modules import pin
from modules import cryptIt
from pathlib import Path
import time
import getpass

#getSteemData.main()
print('Starting Nebulus')
print('Please enter your secret phrase.')
passwd = getpass.getpass()

if Path('/root/.ipfs/nebulus-data/.userData').exists:
    print('Using saved user data file.')
else:
    cryptIt.setData(passwd)

#TODO: implement a clean exit mechanism
while True:
    print('Executing IPFS Pinning...')
    pin.main(passwd)
    # TOMAYBE? Make sleep time configurable
    print('Script pausing for one hour...')
    time.sleep(3600)

