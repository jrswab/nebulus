#!/usr/bin/env python3

import sys
import base64
import hashlib
import getpass
from pathlib import Path
from cryptography.fernet import Fernet

def makeKey(passwd):
    # make and return a portion of an SHA512 hash
    # passwd comes from user input in setData() or getData()
    fullHash = hashlib.sha512(passwd.encode()).hexdigest()
    return base64.urlsafe_b64encode(fullHash[12:44].encode())

def encrypt(key, userData):
    f = Fernet(key)
    return f.encrypt(userData.encode())

def decrypt(key):
    f = Fernet(key)

    # open file with encrpyted user data
    # replace any new lines with nothing
    with open('/root/.ipfs/nebulus-data/.userData') as userFile:
        data = userFile.read().replace('\n', '')

    # change data to bitwise for decryption
    # decode the output to a string and split it into a list
    return f.decrypt(data.encode()).decode().split('\n')

def checkFile():
    if Path('/root/.ipfs/nebulus-data/.userData').is_file:
        return True
    else:
        return False

def makeFile(encData):
    # write encrypted data to file for storage
    with open('/root/.ipfs/nebulus-data/.userData', 'w') as passFile:
        passFile.write(encData.decode())

def setData(passwd):
    print('Please choose a password to encrypt your'
    + 'Steem account data:')

    # make the key for Fernet based on password input
    key = makeKey(passwd)
    print('Enter Steem User Name (no "@"):')
    account = input()
    
    print('Enter Private Posting Key (WARNING: Use no other key!)')
    # use getpass to hide user input
    wif = getpass.getpass('Posting Key:')

    userData = account + '\n' + wif
    # creat the file containing the encrypted user info
    makeFile(encrypt(key, userData))

def getData(passwd):
    if Path('/root/.ipfs/nebulus-data/.userData').exists:
        # get password
        print('Input your encryption password')

        # make key for decryption
        key = makeKey(passwd)
        return decrypt(key)
    else:
        print('No STEEM data found! Please initialize.')

if __name__ == '__main__':
    print('\nWARNING: This will overwrite your encrypted user data' +
            ' and start from scratch.\nPress ctrl+c to cancel. Or' +
            ' follow the prompt to update.\n\n')
    passwd = getpass.getpass()
    setData(passwd)
    
