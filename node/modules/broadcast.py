#!/usr/bin/env python3

from beem import Steem
from beem.account import Account

def send(passwd):
    # needed to form dictionary from userData
    userFormat = ['user', 'wif']

    # get steem user data
    steemInfo = dict(zip(userFormat, cryptIt.getData(passwd)))

    # set up steem key and account
    steem = Steem(
            nobroadcast=False, # set to true for testing
            keys=[steemInfo['wif']]
    )

    # set acc to the steem account in config/steemAcc
    acc = Account(steemInfo['user'], steem_instance=steem)

    # get pinned hashes
    with open('config/hashList', 'r') as hashList:
        hashes = hashList.read().split('\n')

    # send custom json with hashes
    steem.custom_json('nebulus_req', '{"hashes": ' 
        + ', '.join(hashes) + '"}', 
        required_posting_auths=[acc['name']])

    with open('config/hashList', 'w') as wipe:
        wipe.write('')

if __name__ == '__main__':
    print('Sorry, only runs as a module')
